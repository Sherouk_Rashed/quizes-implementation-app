var getNextSibling = function (elem, selector) {

	// Get the next sibling element
	var sibling = elem.nextElementSibling;

	// If there's no selector, return the first sibling
	if (!selector) return sibling;

	// If the sibling matches our selector, use it
	// If not, jump to the next sibling and continue the loop
	while (sibling) {
		if (sibling.matches(selector)) return sibling;
		sibling = sibling.nextElementSibling
	}

};

var getPreviousSibling = function (elem, selector) {

	// Get the previous sibling element
	
	var sibling = elem.previousElementSibling;
	
	// If there's no selector, return the first sibling
	if (!selector) return sibling;

	// If the sibling matches our selector, use it
	// If not, jump to the previous sibling and continue the loop
	while (sibling) {
		if (sibling.matches(selector)) return sibling;
		sibling = sibling.previousElementSibling
	}

};

var getQuestions = function (elem) {

	var queryChildren = [];
	var allChildren = [];
	if( elem[0] ) {
		allChildren = elem[0].childNodes
	}else{
		allChildren = elem.childNodes
	}
 

	for ( var i = 0 ; i < allChildren.length ; i++ ) {
		if (  allChildren[i].classList)   {
			if (  allChildren[i].classList.contains("template1_v2")  ){
				allChildren[i].childNodes.forEach(element => {
					if (  element.classList  ) {
						if (  element.classList.contains("question")  ) {
							queryChildren.push(element)
						}
					}
				});

				
			}
		}
	}
	return queryChildren;
	
}