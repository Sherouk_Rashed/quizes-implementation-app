
var audio = new Audio() ;

function fnstopAllAudios() {
	audio.pause();
}

function fnResetQuestions( questions ) {
	questions.forEach(question => {
		//..... Mark As notCompleted
		if (question.classList.contains("completed")) {
			question.classList.remove("completed")
			question.classList.add("notCompleted")
		}
		var possibleAnswerOptions = question.childNodes.forEach(option => {
			if (  option.classList  ) {
						// Remove TickMark on Correct Answer
				if (  option.getAttribute("data-Answer") == "correct" ) {
					option.classList.remove("correctTick")
				}
						// Remove Remove disability on Wrong Answer
				if (  option.getAttribute("data-Answer") == "incorrect" ) {
					option.classList.remove("disabled")
				}
			}
		});
	});
}
function fnResetQuestionsItems( className ) {
	var questionsItems = document.getElementsByClassName(className);
	if (questionsItems.length > 1) {
		questionsItems = Array.from(questionsItems)
		questionsItems.forEach(questionItem => {
			questionItem.classList.remove("active")
						//........... Reset each Question
			var questions = getQuestions(questionItem)
			fnResetQuestions(questions)

		});
		// .......... Move To first Question.....
	
		
		questionsItems[0].classList.add("active")
		fnUpdatePageNumber("resetAllBtn")
		$(".backBtn").addClass("disabled")
		$(".nextBtn").removeClass("disabled") 

	}else{
		var questions = getQuestions(questionsItems)
		fnResetQuestions(questions)
		
	}
}

function fnShowAnswers() {
	var currentQuestionItem = document.getElementsByClassName("active item");
	var questions = getQuestions(currentQuestionItem)
	// document.getElementsByClassName("showAnsBtn")[0].classList.add("disabled")
	questions.forEach(element => {
		if( element.classList.contains("notCompleted")) {
			var possibleAnswerOptions = element.childNodes.forEach(option => {
				if (  option.classList  ) {
					if (  option.getAttribute("data-Answer") == "correct" ) {
						fnShowRightAnswers( option )
					}
				}
			});
		}
	});	
}

function fnShowRightAnswers( correctAnswer ) {
			// Change Question Completion state (classes: complete/ inComplete)
	correctAnswer.parentElement.classList.remove("notCompleted")
	correctAnswer.parentElement.classList.add("completed")
			// Add Classes correctTick Or incorrectTick
	correctAnswer.classList.add("correctTick")
			// disable Wrong Answer
	var wrongAnswer = getNextSibling(correctAnswer, ".option") ? getNextSibling(correctAnswer, ".option") : getPreviousSibling(correctAnswer, ".option")
	wrongAnswer.classList.add("disabled")
			// Change Visiblity of Show Answer Btn	
	fnUpdateShowAnsBtnVisibility()
}

function fnUpdateShowAnsBtnVisibility() {
	var currentQuestion = document.getElementsByClassName("active item");
	var questions = getQuestions(currentQuestion)
	document.getElementsByClassName("showAnsBtn")[0].classList.add("disabled")
	questions.forEach(question => {
		if( question.classList.contains("notCompleted")) {
			document.getElementsByClassName("showAnsBtn")[0].classList.remove("disabled")
			return
		}
	});
}

function fnWrongAnswerSubmitted( wrongAnswer) {
	if(	wrongAnswer.parentElement.classList.contains("notCompleted")	) {
				// Stop Any Sound Effect
		fnstopAllAudios()
				// Play Suitble sound effects
		fnPlaySoundAudio(wrongAnswer.getAttribute("data-audioSrc"))
				// Add Classes correctTick Or incorrectTick
		wrongAnswer.classList.add("incorrectTick")
		setTimeout(function(){
			wrongAnswer.classList.remove("incorrectTick")
			}, 2000);
	}
}

function fnRightAnswerSubmitted( correctAnswer ) {
			// Change Question Completion state (classes: complete/ inComplete)
	if(	correctAnswer.parentElement.classList.contains("notCompleted")	) {
		correctAnswer.parentElement.classList.remove("notCompleted")
		correctAnswer.parentElement.classList.add("completed")
				// Stop Any Sound Effect
				fnstopAllAudios()
				// Play Suitble sound effects
		fnPlaySoundAudio(correctAnswer.getAttribute("data-audioSrc"))
				// Add Classes correctTick Or incorrectTick
		correctAnswer.classList.add("correctTick")
				// disable Wrong Answer
		var wrongAnswer = getNextSibling(correctAnswer, ".option") ? getNextSibling(correctAnswer, ".option") : getPreviousSibling(correctAnswer, ".option")
		wrongAnswer.classList.add("disabled")	
	}		
	
}

function fnPlaySoundAudio( audioName ) {
	audio = new Audio(audioName);
	audio.play();
}

function fnTitleAudioClick()
{
			// Action of speaker button goes here
	
}

function fnUpdatePageNumber( className ) 
{
	var pageCounter = document.getElementsByClassName("pageNumber")[0];
	var pageNumber = fnGetNumberOfCurrentQuestions();
	var numberOfQuestions = document.getElementsByClassName("pageNumber")[0].getAttribute("questions");

	switch (className) {
		case "nextBtn":
			pageCounter.innerHTML = (pageNumber + 1) + " of " + numberOfQuestions
			break;

		case "backBtn":
			pageCounter.innerHTML = (pageNumber - 1) + " of " + numberOfQuestions
			break;

		case "resetAllBtn":
			pageCounter.innerHTML = "1" + " of " + numberOfQuestions
			break;
		default:
			break;
	}
}

function fnUpdateQuestion( className ) 
{

	var currentQuestion = document.getElementsByClassName("item active")[0];

	switch (className) {
		case "nextBtn":
			var nextQuestion = getNextSibling(currentQuestion, '.item');
			currentQuestion.classList.remove("active");
			nextQuestion.classList.add("active")
			break;

		case "backBtn":
			var previousQuestion = getPreviousSibling(currentQuestion, '.item');
			currentQuestion.classList.remove("active");
			previousQuestion.classList.add("active")
			break;
	
		default:
			break;
	}
}

function fnElementIsDisabled( className )
{
						// Check if Element is Disabled Here
	var numberOfQuestions = document.getElementsByClassName("pageNumber")[0].getAttribute("questions");

	switch (className) {
		case "nextBtn":
			if ( numberOfQuestions == fnGetNumberOfCurrentQuestions() ){
				return true
			}
			break;

		case "backBtn":
			if ( fnGetNumberOfCurrentQuestions() == 1 ){
				return true
			}
			break;
	
		default:
			break;
	}

	return false
	
}

function fnGetNumberOfCurrentQuestions () 
{
	var pageCounter = document.getElementsByClassName("pageNumber")[0].innerHTML;
	var pageNumber = parseInt(pageCounter.split(" ")[0]);
	return pageNumber
}