
                            // Scaling Content
function doResize() {
  
  var $child = $(".page_content"); 
  var $container = $("#container"); 
  var childHeight = $child.height();
  var childWidth = $child.width();
  var containerHeight = $container.height();
  var containerWidth = $container.width();
  var scale, left;

  scale = Math.min(
    containerWidth / childWidth,    
    containerHeight / childHeight
  );
  
  left = (0.5*containerWidth) - (0.5*(childWidth*scale))
  left < 0 ? left = 0 : left = left
  $child.css("transform" , "scale(" + scale + ")");
  $child.css("left" , left + "px");
  $child.css("top" ,"0px");
  
}


doResize()

window.onresize = function() {
    doResize()
}