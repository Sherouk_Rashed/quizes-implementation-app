$(document).ready(function(){
    
        // Intially Hidden Popups
    $('.resoursePopup').hide();
    $('.helpPopup').hide();

        // Press Next Question Action
    $(".nextBtn").click(function() {
        if ( ! $(this).hasClass("disabled") ) {
            // Stop Any Sound Effect
            fnstopAllAudios()

            fnUpdateQuestion("nextBtn");
            fnUpdatePageNumber("nextBtn");
            fnUpdateShowAnsBtnVisibility();
        }
        fnElementIsDisabled("nextBtn") ? $(".nextBtn").addClass("disabled") : $(".backBtn").removeClass("disabled");

    })

        // Press Previous Question Action
    $(".backBtn").click(function() {
        if ( ! $(this).hasClass("disabled") ) {
            // Stop Any Sound Effect
            fnstopAllAudios()

            fnUpdateQuestion("backBtn");
            fnUpdatePageNumber("backBtn");
            fnUpdateShowAnsBtnVisibility();
        }
        fnElementIsDisabled("backBtn") ? $(".backBtn").addClass("disabled") : $(".nextBtn").removeClass("disabled")

    })

            // Show Resourse And Help Popups
    $(".resourseBtn").click(function() {
        $(".resourseContainer").css("height", "725.4px")
        $('.resoursePopup').show();
    })

    $(".helpBtn").click(function() {
        $(".helpContainer").css("height", "725.4px")
        $('.helpPopup').show();
    })

            // Hide Resourse And Help Popups
    $(".closeBtn").click(function() {
        if(  $(".resoursePopup").is(":visible")  ){
        $('.resoursePopup').hide();
        }else{
        $('.helpPopup').hide();
        }

    })

            // Selecting an Answer Action
    $(".option").click(function () {
        if ( $(this).attr("data-Answer") == "correct" ) {
            fnRightAnswerSubmitted( $(this)[0] )
        }else if ( $(this).attr("data-Answer") == "incorrect" )	{
            if ( ! $(this).hasClass("disabled") ) {
                fnWrongAnswerSubmitted( $(this)[0] )
            }
        }	
        fnUpdateShowAnsBtnVisibility()
    })

                // Show Answers Btn Action
        $(".showAnsBtn").click(function () {
        if ( ! $(this).hasClass("disabled") ) {
        // Stop Any Sound Effect
        fnstopAllAudios()

        fnShowAnswers()
    }
    })
        // Reset All Questions
    $(".reloadBtnAll").click(function () {
        // Stop Any Sound Effect
        fnstopAllAudios()

        fnResetQuestionsItems("item")
        fnUpdateShowAnsBtnVisibility()	
    })
        // Reset Only Active Questions

    $(".reloadBtnScreen").click(function () {
        // Stop Any Sound Effect
        fnstopAllAudios()

        fnResetQuestionsItems("item active")
        fnUpdateShowAnsBtnVisibility()
    })


})